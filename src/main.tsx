import ReactDOM from "react-dom/client";
import router from "@/routes";
import { RouterProvider } from "react-router-dom";
import {theme} from "@/theme";
import { ThemeProvider } from "@mui/material/styles";
import {
  Experimental_CssVarsProvider as CssVarsProvider,
} from '@mui/material/styles';

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <>
    <CssVarsProvider theme={theme}>
      <RouterProvider router={router} />
    </CssVarsProvider>
  </>
);
