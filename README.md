# dualnback-app

A dual n-back implementation in react to train your working memory.

# How to play

Remember a sequence of sounds/positions to select wether they showed up n-steps back.

![2_back_tutorial](./src/assets/2back_tutorial.png)
![3_back_tutorial](./src/assets/3back_tutorial.png)

# Development

```
npm install
npm run dev
```
