import "@/App.css";
import { useEffect, useState, useRef } from "react";
import useLocalStorage from "@/hooks/useLocalStorage";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import { Settings, defaultSettings } from "@/models/Settings";
import { Typography } from "@mui/material";
import {
  Session,
  getSession,
  isSoundCorrect,
  isVisualCorrect,
  getSoundFeedbackBg,
  getVisualFeedbackBg,
} from "@/models/Sessions";
import { NbackItem } from "@/models/Nback";
import { States } from "@/models/States";
import sounds from "@/assets/sounds/sounds";
import { Box, Grid, Tooltip } from "@mui/material";
import Snackbars from "@/components/Snackbars";
import TopBar from "@/components/TopBar";

function App() {
  const theme = useTheme();
  const isDesktopScreen = useMediaQuery(theme.breakpoints.up("md"));
  const [nbackSequence, setNbackSequence] = useState<NbackItem[]>([]);
  const [state, setState] = useState<States>(States.Idle);
  const [sequenceIndex, setSequenceIndex] = useState(0);
  const stateRef = useRef(0);
  stateRef.current = state;
  const [showNbackBlock, setShowNbackBlock] = useState(true);
  const [showFeedback, setShowFeeback] = useState(false);
  const [sessions, setSessions] = useLocalStorage<Session[]>(
    "sessionsHistory",
    []
  );

  const [settings, setSettings] = useLocalStorage<Settings>(
    "settings",
    defaultSettings
  );

  const [level, setLevel] = useLocalStorage<number>(
    "level",
    settings.defaultLevel
  );
  const levelRef = useRef(0);
  levelRef.current = level;

  const intervalRef = useRef<NodeJS.Timer>();
  const selectionsEventListenerRef = useRef<(event: KeyboardEvent) => void>();

  const [openFeedbackSnackbar, setOpenFeedbackSnackbar] = useState(false);
  const [openStopSnackbar, setOpenStopSnackbar] = useState(false);
  const handleCloseFeedbackSnackbar = () => {
    setOpenFeedbackSnackbar(false);
  };
  const handleCloseStopSnackbar = () => {
    setOpenStopSnackbar(false);
  };

  function selectionsEventListener(event: KeyboardEvent, index: number) {
    // Cannot select yet
    if (index < level) return;

    switch (event.key) {
      case "a": // visual
        visualSelection(index);
        break;
      case "l": // sound
        soundSelection(index);
        break;
    }
  }

  function visualSelection(index: number) {
    // Cannot select yet
    if (index < level) return;

    setNbackSequence((sequence) => {
      let newSequence = [...sequence];
      newSequence[index]["visualSelection"] = true;
      return newSequence;
    });
  }

  function soundSelection(index: number) {
    // Cannot select yet
    if (index < level) return;

    setNbackSequence((sequence) => {
      let newSequence = [...sequence];
      newSequence[index]["soundSelection"] = true;
      return newSequence;
    });
  }

  function shortcutsEventListener(event: KeyboardEvent) {
    switch (event.key) {
      case " ":
        if (stateRef.current == States.Idle) {
          initNback();
        }
        break;
      case "Escape":
        if (stateRef.current == States.Running) {
          stopNback();
        }
        break;
    }
  }

  function initNback() {
    // Create random sequence of NbackItems
    const sequence = [] as NbackItem[];
    let i = 0;
    while (i < settings.rounds * levelRef.current) {
      sequence.push({
        visual: randomIntFromInterval(0, 8),
        sound: randomIntFromInterval(0, 7),
        visualSelection: false,
        soundSelection: false,
        isVisualCorrect: false,
        isSoundCorrect: false,
      });
      i += 1;
    }
    setNbackSequence(sequence);

    // Enter training mode
    setState(States.Running);
    // Start
    setSequenceIndex(0);
  }

  function stopNback() {
    // Discard current session
    setState(States.Idle);
    setSequenceIndex(-1); // clear screen
    setOpenStopSnackbar(true);
  }

  useEffect(() => {
    window.addEventListener("keydown", shortcutsEventListener);
  }, []);

  useEffect(() => {
    switch (state) {
      case States.Running:
        // Set interval function to increment sequenceIndex periodically
        const intervalId = setInterval(() => {
          setSequenceIndex((prevIndex) => {
            if (prevIndex == settings.rounds * level - 1) {
              setState(States.Finished);
              return prevIndex;
            }
            return prevIndex + 1;
          });
        }, settings.delayTime);

        intervalRef.current = intervalId;
        break;

      case States.Finished:
        const session = getSession(nbackSequence, level);
        setSessions((sessions: Session[]) => {
          return [...sessions, session];
        });
        setLevel(session.newLevel);
        setSequenceIndex(-1); // clear screen
        setOpenFeedbackSnackbar(true);
        setState(States.Idle);
        break;
    }

    return () => {
      clearInterval(intervalRef.current);
    };
  }, [state]);

  // Visual and sound selection event listeners
  useEffect(() => {
    // |     1    |     2     |     3     |     4     |     5     |     6     |
    // | Show block           |  hidden block         | feedback  |endFeedback|
    // |       Selection time                         |
    // |                 Delay time                                           |
    if (state == States.Running) {
      const hideBlockTimeOffset = (settings.delayTime * 2) / 5;
      const endSelectionTimeOffset = (settings.delayTime * 4) / 5;
      const feedbackTimeOffset = endSelectionTimeOffset;
      const endFeedbackTimeOffset = settings.delayTime - 100;

      // Play sound
      const soundIndex = nbackSequence[sequenceIndex]?.sound;
      if (soundIndex !== null && sequenceIndex >= 0) {
        sounds[soundIndex].play();
      }

      // Flash nbackblock
      setShowNbackBlock(true);
      setTimeout(() => setShowNbackBlock(false), hideBlockTimeOffset);

      // Selection feedback
      setShowFeeback(false);
      if (sequenceIndex >= level) {
        setTimeout(() => {
          setNbackSequence((sequence) => {
            let newSequence = [...sequence];
            newSequence[sequenceIndex]["isVisualCorrect"] = isVisualCorrect(
              newSequence,
              sequenceIndex,
              level
            );
            newSequence[sequenceIndex]["isSoundCorrect"] = isSoundCorrect(
              newSequence,
              sequenceIndex,
              level
            );
            return newSequence;
          });
          setShowFeeback(true);
        }, feedbackTimeOffset);

        // Hide feedback just before jumping to the next sequenceIndex
        setTimeout(() => setShowFeeback(false), endFeedbackTimeOffset);
      }

      // Selection time window
      selectionsEventListenerRef.current = (event) =>
        selectionsEventListener(event, sequenceIndex);
      window.addEventListener("keydown", selectionsEventListenerRef.current);
      setTimeout(() => {
        if (selectionsEventListenerRef.current) {
          window.removeEventListener(
            "keydown",
            selectionsEventListenerRef.current
          );
        }
      }, endSelectionTimeOffset);
    }

    // Cleanup function: remove event listener
    return () => {
      if (selectionsEventListenerRef.current) {
        window.removeEventListener(
          "keydown",
          selectionsEventListenerRef.current
        );
      }
    };
  }, [state, sequenceIndex]);

  return (
    <>
      <Grid
        container
        direction="column"
        sx={{
          height: "100vh",
          backgroundColor: "background.paper",
          maxHeight: "100vh",
          overflow: "hidden",
        }}
      >
        {/* Menu bar */}
        <Grid item container justifyContent="center" alignItems="center" xs={1}>
          <TopBar
            level={level}
            state={state}
            settings={settings}
            sessions={sessions}
            setState={setState}
            setSettings={setSettings}
            initNback={initNback}
            stopNback={stopNback}
          />
        </Grid>

        {/* Square */}
        <Grid
          container
          item
          xs={7}
          md={10}
          justifyContent="center"
          alignItems="center"
        >
          <Grid
            xs={11}
            md={5}
            item
            container
            sx={{
              height: isDesktopScreen ? "40vw" : "80vw",
              width: isDesktopScreen ? "40vw" : "80vw",
            }}
          >
            {[...Array(9).keys()].map((i) => {
              return (
                <Grid item xs={4} key={i}>
                  <Box
                    sx={{
                      height: "100%",
                      borderTop: 3,
                      borderRight: 3,
                      borderLeft: i % 3 == 0 ? 3 : 0,
                      borderBottom: i / 3 >= 2 ? 3 : 0,
                      borderColor: "secondary.main",
                      backgroundColor:
                        i == nbackSequence[sequenceIndex]?.visual &&
                        showNbackBlock
                          ? "primary.main"
                          : "transparent",
                      transition: "background-color 0.2s ease",
                    }}
                  />
                </Grid>
              );
            })}
          </Grid>
        </Grid>

        {/* Action buttons */}
        <Grid
          container
          item
          xs={true}
          justifyContent="space-between"
          alignItems="center"
          sx={{ marginTop: isDesktopScreen ? "-10vh" : "5vh" }}
        >
          <Tooltip title="Press A for visual match">
            <Grid
              onClick={() => visualSelection(sequenceIndex)}
              item
              container
              alignItems="center"
              justifyContent="center"
              xs={isDesktopScreen ? 2 : 5}
              sx={{
                cursor: "pointer",
                height: "100%",
                borderTop: 3,
                borderRight: 3,
                borderColor: "secondary.main",
                borderTopRightRadius: isDesktopScreen ? "100%" : "50%",
                backgroundColor: getVisualFeedbackBg(
                  nbackSequence,
                  sequenceIndex,
                  showFeedback,
                  settings.showSuccessFeedback,
                  settings.showErrorFeedback
                ),
              }}
            >
              <Grid
                item
                sx={{
                  marginRight: "2rem",
                  marginTop: "2rem",
                  color: "text.secondary",
                }}
              >
                <Typography variant="subtitle2">A</Typography>
              </Grid>
            </Grid>
          </Tooltip>

          <Tooltip title="Press L for sound match">
            <Grid
              onClick={() => soundSelection(sequenceIndex)}
              item
              container
              alignItems="center"
              justifyContent="center"
              xs={isDesktopScreen ? 2 : 5}
              sx={{
                cursor: "pointer",
                height: "100%",
                borderTop: 3,
                borderLeft: 3,
                borderColor: "secondary.main",
                borderTopLeftRadius: isDesktopScreen ? "100%" : "50%",
                backgroundColor: getSoundFeedbackBg(
                  nbackSequence,
                  sequenceIndex,
                  showFeedback,
                  settings.showSuccessFeedback,
                  settings.showErrorFeedback
                ),
              }}
            >
              <Grid
                item
                sx={{
                  marginLeft: "2rem",
                  marginTop: "2rem",
                  color: "text.secondary",
                }}
              >
                <Typography variant="subtitle2">L</Typography>
              </Grid>
            </Grid>
          </Tooltip>
        </Grid>
      </Grid>

      <Snackbars
        session={sessions[sessions.length - 1]}
        openFeedbackSnackbar={openFeedbackSnackbar}
        openStopSnackbar={openStopSnackbar}
        handleCloseFeedbackSnackbar={handleCloseFeedbackSnackbar}
        handleCloseStopSnackbar={handleCloseStopSnackbar}
      />
    </>
  );
}

function randomIntFromInterval(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export default App;
