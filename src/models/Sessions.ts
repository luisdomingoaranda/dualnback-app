import { NbackItem } from "@/models/Nback";
import { AlertColor } from "@mui/material/Alert/Alert";

export type Session = {
  date: Date;
  visualAccuracy: number;
  soundAccuracy: number;
  currentLevel: number;
  newLevel: number;
};

function getNewLevel(
  currentLevel: number,
  visualAccuracy: number,
  soundAccuracy: number
): number {
  let newLevel = currentLevel;

  if (visualAccuracy > 0.95 && soundAccuracy > 0.95) {
    newLevel += 1;
  }

  if (visualAccuracy + soundAccuracy < 1.75) {
    newLevel -= 1;
  }

  // Minimum level is 1
  if (newLevel < 1) {
    return 1;
  }

  return newLevel;
}

export function isSoundCorrect(
  sequence: NbackItem[],
  sequenceIndex: number,
  level: number
) {
  const soundMatch =
    sequence[sequenceIndex].sound == sequence[sequenceIndex - level].sound;
  if (sequence[sequenceIndex].soundSelection == soundMatch) {
    return true;
  } else {
    return false;
  }
}

export function isVisualCorrect(
  sequence: NbackItem[],
  sequenceIndex: number,
  level: number
) {
  const visualMatch =
    sequence[sequenceIndex].visual == sequence[sequenceIndex - level].visual;
  if (sequence[sequenceIndex].visualSelection == visualMatch) {
    return true;
  } else {
    return false;
  }
}

function getAccuracies(sequence: NbackItem[], level: number) {
  // Set "isCorrect" fields
  for (let i in sequence) {
    const index = Number(i);

    if (index < level) {
      continue;
    }

    const visualMatch =
      sequence[index].visual == sequence[index - level].visual;
    const soundMatch = sequence[index].sound == sequence[index - level].sound;

    if (sequence[index].soundSelection == soundMatch) {
      sequence[index].isSoundCorrect = true;
    }

    if (sequence[index].visualSelection == visualMatch) {
      sequence[index].isVisualCorrect = true;
    }
  }

  // Compute accuracies
  const nVisualCorrects = sequence.filter(
    (item) => item.isVisualCorrect
  ).length;
  const nSoundCorrects = sequence.filter((item) => item.isSoundCorrect).length;
  const maxCorrects = sequence.length - level;

  return [nVisualCorrects / maxCorrects, nSoundCorrects / maxCorrects];
}

export function getSession(
  sequence: NbackItem[],
  currentLevel: number
): Session {
  const [visualAccuracy, soundAccuracy] = getAccuracies(sequence, currentLevel);
  const newLevel = getNewLevel(currentLevel, visualAccuracy, soundAccuracy);

  return {
    date: new Date(),
    visualAccuracy: visualAccuracy,
    soundAccuracy: soundAccuracy,
    currentLevel: currentLevel,
    newLevel: newLevel,
  };
}

export function getVisualFeedbackBg(
  sequence: NbackItem[],
  sequenceIndex: number,
  showFeedback: boolean,
  showSuccessFeedback: boolean,
  showErrorFeedback: boolean
) {
  if (showFeedback) {
    if (showSuccessFeedback && sequence[sequenceIndex]?.isVisualCorrect) {
      return "success.main";
    }

    if (
      showErrorFeedback &&
      sequence[sequenceIndex]?.isVisualCorrect == false
    ) {
      return "warning.main";
    }
  }

  if (sequence[sequenceIndex]?.visualSelection) {
    return "primary.main";
  } else {
    return "transparent";
  }
}

export function getSoundFeedbackBg(
  sequence: NbackItem[],
  sequenceIndex: number,
  showFeedback: boolean,
  showSuccessFeedback: boolean,
  showErrorFeedback: boolean
) {
  if (showFeedback) {
    if (showSuccessFeedback && sequence[sequenceIndex]?.isSoundCorrect) {
      return "success.main";
    }

    if (showErrorFeedback && sequence[sequenceIndex]?.isSoundCorrect == false) {
      return "warning.main";
    }
  }

  if (sequence[sequenceIndex]?.soundSelection) {
    return "primary.main";
  } else {
    return "transparent";
  }
}

export function getSnackbarInfo(session: Session): {
  severity: AlertColor;
  message: string;
} {
  if (session == undefined) {
    return { severity: "error", message: "Error" };
  }

  if (session.newLevel > session.currentLevel) {
    return { severity: "success", message: "Level up! Well done!" };
  }

  if (session.newLevel === session.currentLevel) {
    return { severity: "info", message: "Same level. Keep pushing!" };
  }

  if (session.newLevel < session.currentLevel) {
    return { severity: "warning", message: "Level down. Let's bounce back!" };
  }

  return { severity: "error", message: "Error" };
}

export function getTodaySessions(sessions: Session[]) {
  const todaySessions = sessions.filter((session: Session) => {
    const today = new Date();
    const sessionDate = new Date(session.date);
    return today.getDate() === sessionDate.getDate()
      && today.getMonth() === sessionDate.getMonth()
      && today.getFullYear() === sessionDate.getFullYear();
  });

  return todaySessions;
}
