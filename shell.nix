{ pkgs ? import <nixpkgs> {} }:

let
  node = pkgs.nodejs-18_x; # Choose the Node.js version

in pkgs.mkShell {
  name = "nback app";

  buildInputs = [ node ];

  shellHook = ''
    export NODE_ENV=development
    export PATH="${node}/bin:$PATH"

    if ! [ -d node_modules ]; then
      echo "Installing dependencies..."
      ${node}/bin/npm install
    fi

    echo "Environment is ready. You can now run the following commands:"
    echo "- npm run dev: Start the development server"
    echo "- npm run build: Build the project for production"
    echo "- npm test: Run tests"
    echo "- npm format: Format files with prettier"
  '';
}
