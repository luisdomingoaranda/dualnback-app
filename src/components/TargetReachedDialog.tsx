import { DialogTitle, Grid, IconButton, Typography } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import CloseIcon from "@mui/icons-material/Close";

type TargetReachedDialogProps = {
  open: boolean;
  handleClose: () => void;
};

function TargetReachedDialog(props: TargetReachedDialogProps) {
  return (
    <>
      <Dialog open={props.open} onClose={props.handleClose}>
        <DialogTitle>
          <Grid container justifyContent="end">
            <IconButton onClick={props.handleClose}>
              <CloseIcon sx={{ color: "secondary.main" }} />
            </IconButton>
          </Grid>
        </DialogTitle>
        <DialogContent>
          <Grid
            container
            direction="column"
            alignItems="center"
            justifyContent="center"
            spacing={2}
            p={5}
          >
            <Grid item>
              <Typography variant="h1" component="div">
                💪
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="h6" align="center">
                Congrats!
              </Typography>
              <Typography variant="subtitle2">
                You've reached your daily target!
              </Typography>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </>
  );
}

export default TargetReachedDialog;
