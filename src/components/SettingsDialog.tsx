import React from "react";
import { useForm, Controller } from "react-hook-form";
import { useState } from "react";
import { Grid, IconButton, Slide, Snackbar, Tooltip } from "@mui/material";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import { Settings } from "@/models/Settings";
import { TextField, Switch, FormControlLabel, Button } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

type SettingsDialogProps = {
  open: boolean;
  handleClose: () => void;
  settings: Settings;
  setSettings: (settings: Settings) => void;
};

function SettingsDialog(props: SettingsDialogProps) {
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    defaultValues: props.settings,
    mode: "onChange",
  });

  const onSubmit = (data: Settings) => {
    const parsedData = {
      ...data,
      defaultLevel: Number(data.defaultLevel),
      delayTime: Number(data.delayTime),
      rounds: Number(data.rounds),
      dailyTarget: Number(data.dailyTarget),
    };
    props.setSettings(parsedData);
    setOpenSnackbar(true);
  };

  return (
    <>
      <Dialog open={props.open} onClose={props.handleClose}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogTitle>
            <Grid container justifyContent="space-between">
              <Typography variant="h6" gutterBottom>
                Settings
              </Typography>
              <IconButton onClick={props.handleClose}>
                <CloseIcon sx={{ color: "secondary.main" }} />
              </IconButton>
            </Grid>
          </DialogTitle>
          <DialogContent>
            <Grid
              container
              sx={{ padding: "2rem" }}
              spacing={2}
              justifyContent="center"
              direction="column"
            >
              <Grid item container xs={12} alignItems="center" columnSpacing={1}>
                <Grid item xs={11}>
                  <TextField
                    {...register("defaultLevel", {
                      required: { value: true, message: "Required field" },
                      min: { value: 1, message: "Minimum value is 1" },
                    })}
                    error={Boolean(errors.defaultLevel)}
                    helperText={errors.defaultLevel?.message as string}
                    label="Default Level"
                    type="number"
                  />
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title="Starting level">
                    <InfoOutlinedIcon />
                  </Tooltip>
                </Grid>
              </Grid>

              <Grid item container xs={12} alignItems="center" columnSpacing={1}>
                <Grid item xs={11}>
                  <TextField
                    {...register("delayTime", {
                      required: { value: true, message: "Required field" },
                      min: { value: 1000, message: "Minimum value is 1000 ms" },
                    })}
                    error={Boolean(errors.delayTime)}
                    helperText={errors.delayTime?.message as string}
                    label="Delay Time (ms)"
                    type="number"
                  />
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title="Time between sequence items in milliseconds">
                    <InfoOutlinedIcon />
                  </Tooltip>
                </Grid>
              </Grid>

              <Grid item container xs={12} alignItems="center" columnSpacing={1}>
                <Grid item xs={11}>
                  <TextField
                    {...register("rounds", {
                      required: { value: true, message: "Required field" },
                      min: { value: 2, message: "Minimum value is 2 rounds" },
                    })}
                    error={Boolean(errors.rounds)}
                    helperText={errors.rounds?.message as string}
                    label="Rounds"
                    type="number"
                  />
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title="Sequence length is: level * rounds">
                    <InfoOutlinedIcon />
                  </Tooltip>
                </Grid>
              </Grid>

              <Grid item container xs={12} alignItems="center" columnSpacing={1}>
                <Grid item xs={11}>
                  <TextField
                    {...register("dailyTarget", {
                      required: { value: true, message: "Required field" },
                      min: { value: 1, message: "Minimum value is 1" },
                    })}
                    error={Boolean(errors.dailyTarget)}
                    helperText={errors.dailyTarget?.message as string}
                    label="Daily Target"
                    type="number"
                  />
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title="Daily sessions target">
                    <InfoOutlinedIcon />
                  </Tooltip>
                </Grid>
              </Grid>

              <Grid item container xs={12} alignItems="center" columnSpacing={1}>
                <Grid item xs={11}>
                  <Controller
                    name="showErrorFeedback"
                    control={control}
                    render={({ field }) => (
                      <FormControlLabel
                        control={
                          <Switch
                            {...field}
                            checked={field.value}
                            onChange={(e) => field.onChange(e.target.checked)}
                            color="primary"
                          />
                        }
                        label="Show Error Feedback"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title="Error flash after wrong match">
                    <InfoOutlinedIcon />
                  </Tooltip>
                </Grid>
              </Grid>

              <Grid item container xs={12} alignItems="center" columnSpacing={1}>
                <Grid item xs={11}>
                  <Controller
                    name="showSuccessFeedback"
                    control={control}
                    render={({ field }) => (
                      <FormControlLabel
                        control={
                          <Switch
                            {...field}
                            checked={field.value}
                            onChange={(e) => field.onChange(e.target.checked)}
                            color="primary"
                          />
                        }
                        label="Show Success Feedback"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title="Success flash after correct match">
                    <InfoOutlinedIcon />
                  </Tooltip>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={props.handleClose}>Cancel</Button>
            <Button type="submit">Submit</Button>
          </DialogActions>
        </form>
      </Dialog>

      <Snackbar
        open={openSnackbar}
        autoHideDuration={4000}
        onClose={() => setOpenSnackbar(false)}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        TransitionComponent={Slide}
      >
        <Alert
          onClose={() => setOpenSnackbar(false)}
          severity="info"
          sx={{ width: "100%" }}
        >
          <span>Settings updated</span>
        </Alert>
      </Snackbar>
    </>
  );
}

export default SettingsDialog;
