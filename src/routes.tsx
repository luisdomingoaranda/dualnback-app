import App from "./App";
import { createHashRouter } from "react-router-dom";

export const ROUTE_PATHS = {
  home: "/",
};

const router = createHashRouter([
  {
    path: ROUTE_PATHS.home,
    element: <App />,
  },
]);

export default router;
