export interface Settings {
  defaultLevel: number;
  rounds: number;
  delayTime: number;
  dailyTarget: number;
  showErrorFeedback: boolean;
  showSuccessFeedback: boolean;
}

export const defaultSettings = {
  defaultLevel: 1,
  rounds: 5,
  delayTime: 2000,
  dailyTarget: 20,
  showErrorFeedback: true,
  showSuccessFeedback: true,
};
