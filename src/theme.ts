import { experimental_extendTheme as extendTheme} from '@mui/material/styles';

export const theme = extendTheme({
  colorSchemes: {
    light: {
      palette: {
        mode: "light",
        primary: {
          main: "#657b83", // Solarized base00
        },
        secondary: {
          main: "#586e75", // Solarized base01
        },
        error: {
          main: "#dc322f", // Solarized red
        },
        warning: {
          main: "#cb4b16", // Solarized orange
        },
        info: {
          main: "#268bd2", // Solarized blue
        },
        success: {
          main: "#859900", // Solarized green
        },
        background: {
          default: "#fdf6e3", // Solarized base3
          paper: "#eee8d5", // Solarized base2
        },
        text: {
          primary: "#657b83", // Solarized base00
          secondary: "#586e75", // Solarized base01
        },
      },
    },
    dark: {
      palette: {
        mode: "dark",
        primary: {
          main: "#839496", // Solarized base0
        },
        secondary: {
          main: "#93a1a1", // Solarized base1
        },
        error: {
          main: "#dc322f", // Solarized red
        },
        warning: {
          main: "#cb4b16", // Solarized orange
        },
        info: {
          main: "#268bd2", // Solarized blue
        },
        success: {
          main: "#859900", // Solarized green
        },
        background: {
          default: "#002b36", // Solarized base03
          paper: "#073642", // Solarized base02
        },
        text: {
          primary: "#839496", // Solarized base0
          secondary: "#93a1a1", // Solarized base1
        },
      },
    }
  }
});
