import { Grid, IconButton } from "@mui/material";
import { alpha } from "@mui/material";
import { Session } from "@/models/Sessions";
import { useTheme } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Settings } from "@/models/Settings";
import Plot from "react-plotly.js";
import { Typography } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';

type ChartsDialogProps = {
  open: boolean;
  handleClose: () => void;
  sessions: Session[];
  settings: Settings;
};

function ChartsDialog(props: ChartsDialogProps) {
  const theme = useTheme();

  // Group by date and collect currentLevels
  const levelsByDate: Record<string, number[]> = props.sessions.reduce(
    (acc: Record<string, number[]>, { date, currentLevel }: Session) => {
      const day = new Date(date).toISOString().split("T")[0];
      if (!acc[day]) {
        acc[day] = [];
      }
      acc[day].push(currentLevel);
      return acc;
    },
    {}
  );

  const dates = Object.keys(levelsByDate);

  // Compute average currentLevel for each date
  const averageLevelsByDate: Record<string, number> = Object.keys(
    levelsByDate
  ).reduce((acc: Record<string, number>, day: string) => {
    const levels = levelsByDate[day];
    const sum = levels.reduce((a, b) => a + b, 0);
    const avg = sum / levels.length;
    acc[day] = avg;
    return acc;
  }, {});

  const averageLevelsPlot = {
    data: [
      {
        x: dates,
        y: Object.values(averageLevelsByDate), // average levels
        type: "line",
        mode: "lines+markers",
        marker: { color: theme.palette.primary.main },
      },
    ],
    layout: {
      margin: {
        l: 40,
        r: 0,
        t: 0
      },
      plot_bgcolor: "transparent",
      paper_bgcolor: "transparent",
      xaxis: {
        color: theme.palette.primary.main,
        linecolor: theme.palette.primary.main,
        gridcolor: alpha(theme.palette.primary.main, 0.2),
      },
      yaxis: {
        title: "Average Level",
        color: theme.palette.primary.main,
        linecolor: theme.palette.primary.main,
        gridcolor: alpha(theme.palette.primary.main, 0.2),
      },
    },
    style: {
      height: "400px",
      width: "90vw",
    },
  };

  const sessionsNumberPlot = {
    data: [
      {
        x: dates,
        y: Object.values(levelsByDate).map((levelsArray) => levelsArray.length), // number of sessions done that day
        type: "bar",
        marker: { color: theme.palette.primary.main },
      },
    ],
    layout: {
      margin: {
        l: 40,
        r: 0,
        t: 20,
        b: 20
      },
      plot_bgcolor: "transparent",
      paper_bgcolor: "transparent",
      xaxis: {
        color: theme.palette.primary.main,
        linecolor: theme.palette.primary.main,
        gridcolor: alpha(theme.palette.primary.main, 0.2),
      },
      yaxis: {
        title: "N sessions",
        color: theme.palette.primary.main,
        linecolor: theme.palette.primary.main,
        range: [0, props.settings.dailyTarget + 10],
        gridcolor: alpha(theme.palette.primary.main, 0.2),
      },
      shapes: [
        {
          type: "line",
          yref: "y",
          y0: props.settings.dailyTarget,
          y1: props.settings.dailyTarget,
          xref: "paper",
          x0: 0,
          x1: 1,
          line: {
            color: theme.palette.primary.main,
            width: 2,
            dash: "dashdot",
          },
        },
      ],
    },
    style: {
      height: "400px",
      width: "90vw",
    },
  };

  return (
    <>
      <Dialog open={props.open} onClose={props.handleClose}>
        <DialogTitle>
          <Grid container justifyContent="space-between">
            <Typography variant="h6" gutterBottom>
              Progress
            </Typography>
            <IconButton onClick={props.handleClose}>
              <CloseIcon sx={{ color: "secondary.main" }} />
            </IconButton>
          </Grid>
        </DialogTitle>
        <DialogContent>
          <Grid container justifyContent="center" alignItems="center">
            <Grid item container xs={12}>
              <Plot
                data={averageLevelsPlot.data}
                layout={averageLevelsPlot.layout}
                style={averageLevelsPlot.style}
              />
            </Grid>
            <Grid item container xs={12}>
              <Plot
                data={sessionsNumberPlot.data}
                layout={sessionsNumberPlot.layout}
                style={sessionsNumberPlot.style}
              />
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </>
  );
}

export default ChartsDialog;
