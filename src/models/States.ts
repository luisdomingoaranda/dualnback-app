export enum States {
  Idle,
  Running,
  Finished,
  Settings,
  Insights,
  Help,
}
