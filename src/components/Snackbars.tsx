import React from "react";
import { Session, getSnackbarInfo } from "@/models/Sessions";
import { Slide, Snackbar } from "@mui/material";
import MuiAlert, { AlertProps } from "@mui/material/Alert";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

type SnackbarsProps = {
  session: Session;
  openFeedbackSnackbar: boolean;
  openStopSnackbar: boolean;
  handleCloseFeedbackSnackbar: () => void;
  handleCloseStopSnackbar: () => void;
};

const Snackbars: React.FC<SnackbarsProps> = (props) => {
  const {
    severity: feedbackSnackbarSeverity,
    message: feedbackSnackbarMessage,
  } = getSnackbarInfo(props.session);

  return (
    <>
      <Snackbar
        open={props.openFeedbackSnackbar}
        autoHideDuration={4000}
        onClose={props.handleCloseFeedbackSnackbar}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        TransitionComponent={Slide}
      >
        <Alert
          onClose={props.handleCloseFeedbackSnackbar}
          severity={feedbackSnackbarSeverity}
          sx={{ width: "100%" }}
        >
          <span>
            {feedbackSnackbarMessage}
            <br />
            {Math.trunc(props.session?.visualAccuracy * 100)}% visual -{" "}
            {Math.trunc(props.session?.soundAccuracy * 100)}% sound
          </span>
        </Alert>
      </Snackbar>

      <Snackbar
        open={props.openStopSnackbar}
        autoHideDuration={4000}
        onClose={props.handleCloseStopSnackbar}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        TransitionComponent={Slide}
      >
        <Alert
          onClose={props.handleCloseStopSnackbar}
          severity="warning"
          sx={{ width: "100%" }}
        >
          <span>Session interrupted</span>
        </Alert>
      </Snackbar>
    </>
  );
};

export default Snackbars;
