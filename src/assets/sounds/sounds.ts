import letter1 from "@/assets/sounds/letter1.mp3";
import letter2 from "@/assets/sounds/letter2.mp3";
import letter3 from "@/assets/sounds/letter3.mp3";
import letter4 from "@/assets/sounds/letter4.mp3";
import letter5 from "@/assets/sounds/letter5.mp3";
import letter6 from "@/assets/sounds/letter6.mp3";
import letter7 from "@/assets/sounds/letter7.mp3";
import letter8 from "@/assets/sounds/letter8.mp3";

const sounds = [
  new Audio(letter1),
  new Audio(letter2),
  new Audio(letter3),
  new Audio(letter4),
  new Audio(letter5),
  new Audio(letter6),
  new Audio(letter7),
  new Audio(letter8),
];

export default sounds;
