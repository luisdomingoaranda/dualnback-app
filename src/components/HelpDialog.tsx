import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import { Grid, IconButton, Typography } from "@mui/material";
import { useColorScheme } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Link from "@mui/material/Link";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import ListItemIcon from "@mui/material/ListItemIcon";
import CloseIcon from '@mui/icons-material/Close';
import Image2BackDark from '@/assets/2back_tutorial_dark.png';
import Image2Back from '@/assets/2back_tutorial.png';
import Image3BackDark from '@/assets/3back_tutorial_dark.png';
import Image3Back from '@/assets/3back_tutorial.png';

type HelpDialogProps = {
  open: boolean;
  handleClose: () => void;
};

function HelpDialog(props: HelpDialogProps) {
  const { mode, setMode } = useColorScheme();
  const image2Back = (mode == "light") ? Image2Back : Image2BackDark;
  const image3Back = (mode == "light") ? Image3Back : Image3BackDark;

  return (
    <>
      <Dialog open={props.open} onClose={props.handleClose}>
        <DialogTitle>
          <Grid container justifyContent="space-between">
            <Typography variant="h6" gutterBottom>
              Help
            </Typography>
            <IconButton onClick={props.handleClose}>
              <CloseIcon sx={{ color: "secondary.main" }} />
            </IconButton>
          </Grid>
        </DialogTitle>
        <DialogContent>
          <Typography variant="body1" gutterBottom>
            Dual N-back is a sequence of two stimuli (position and sound), and
            the task consists of indicating when the current stimulus matches
            the one from N steps earlier in the sequence.
          </Typography>

          <Typography variant="body1" gutterBottom>
            The level factor N is adjusted automatically based on your
            performance to make the task more or less difficult.
          </Typography>

          <Grid container justifyContent="center" alignItems="center">
            <Grid item xs={12}>
              <img src={image2Back} alt="2Back Tutorial" style={{width: '100%', height: '100%'}} />
            </Grid>
            <Grid item xs={12}>
              <img src={image3Back} alt="2Back Tutorial" style={{width: '100%', height: '100%'}} />
            </Grid>
          </Grid>

          <List dense>
            <ListItem>
              <ListItemIcon>
                <ArrowRightIcon fontSize="small" color="primary" />
              </ListItemIcon>
              <ListItemText>
                <Typography variant="subtitle2" gutterBottom>
                  Press <b>A</b> (position match) if the POSITION of the square
                  is the SAME it was N steps back.
                </Typography>
              </ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <ArrowRightIcon fontSize="small" color="primary" />
              </ListItemIcon>
              <ListItemText>
                <Typography variant="subtitle2" gutterBottom>
                  Press <b>L</b> (letter match) if the LETTER you hear is the
                  SAME it was N steps back.
                </Typography>
              </ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <ArrowRightIcon fontSize="small" color="primary" />
              </ListItemIcon>
              <ListItemText>
                <Typography variant="subtitle2" gutterBottom>
                  Press <b>Space</b> to start a session.
                </Typography>
              </ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <ArrowRightIcon fontSize="small" color="primary" />
              </ListItemIcon>
              <ListItemText>
                <Typography variant="subtitle2" gutterBottom>
                  Press <b>Escape</b> to interrupt a session.
                </Typography>
              </ListItemText>
            </ListItem>
          </List>

          <Typography variant="body1" gutterBottom>
            Aim for 20 sessions a day, 4 or 5 times a week.
          </Typography>

          <Typography variant="body1">
            A{" "}
            <Link href="https://www.pnas.org/doi/abs/10.1073/pnas.0801268105">
              2008 study
            </Link>{" "}
            published in the PNAS scientific journal shows that regular dual
            n-back training may improve working memory (short term memory).
          </Typography>
        </DialogContent>
      </Dialog>
    </>
  );
}

export default HelpDialog;
