export interface NbackItem {
  visual: number;
  sound: number;
  visualSelection: boolean;
  soundSelection: boolean;
  isVisualCorrect: boolean;
  isSoundCorrect: boolean;
}
