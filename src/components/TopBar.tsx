import {
  Grid,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
} from "@mui/material";
import { getTodaySessions, Session } from "@/models/Sessions";
import { Settings } from "@/models/Settings";
import { Typography } from "@mui/material";
import { useEffect, useState, useRef } from "react";
import { States } from "@/models/States";
import LightModeIcon from "@mui/icons-material/LightMode";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import HelpIcon from "@mui/icons-material/Help";
import MenuIcon from "@mui/icons-material/Menu";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import StopIcon from "@mui/icons-material/Stop";
import SettingsIcon from "@mui/icons-material/Settings";
import InsightsIcon from "@mui/icons-material/Insights";
import { useColorScheme } from "@mui/material/styles";
import SettingsDialog from "@/components/SettingsDialog";
import HelpDialog from "@/components/HelpDialog";
import ChartsDialog from "@/components/ChartsDialog";
import TargetReachedDialog from "@/components/TargetReachedDialog";

type TopBarProps = {
  level: number;
  state: States;
  settings: Settings;
  sessions: Session[];
  setState: (state: States) => void;
  setSettings: (settings: Settings) => void;
  initNback: () => void;
  stopNback: () => void;
};

function TopBar(props: TopBarProps) {
  const { mode, setMode } = useColorScheme();
  const todaySessions = getTodaySessions(props.sessions);

  const [openSettings, setOpenSettings] = useState(false);
  const handleSettingsClose = () => {
    props.setState(States.Idle);
    setOpenSettings(false);
  };

  const [openHelp, setOpenHelp] = useState(false);
  const handleHelpClose = () => {
    props.setState(States.Idle);
    setOpenHelp(false);
  };

  const [openInsights, setOpenInsights] = useState(false);
  const handleInsightsClose = () => {
    props.setState(States.Idle);
    setOpenInsights(false);
  };

  const [openTargetReached, setOpenTargetReached] = useState(false);
  const handleTargetReachedClose = () => {
    props.setState(States.Idle);
    setOpenTargetReached(false);
  };

  const [anchorMenuEl, setMenuAnchorEl] = useState<null | HTMLElement>(null);
  const handleMenuClick = (event: React.MouseEvent<HTMLElement>) => {
    setMenuAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  useEffect(() => {
    if (todaySessions.length == props.settings.dailyTarget) {
      setOpenTargetReached(true);
    }
  }, [props.sessions]);

  return (
    <>
      <Grid
        item
        container
        xs={11}
        md={8}
        justifyContent="space-around"
        alignItems="center"
        sx={{
          borderRadius: "4vh",
          border: 3,
          borderColor: "secondary.main",
          backgroundColor: "background.default",
        }}
      >
        <Tooltip title="Daily sessions">
          <Grid
            container
            item
            xs={4}
            sx={{ color: "text.primary" }}
            justifyContent="center"
          >
            <Typography variant="subtitle2">
              {todaySessions.length} / {props.settings.dailyTarget}
            </Typography>
          </Grid>
        </Tooltip>

        <Grid
          container
          item
          xs={4}
          sx={{ color: "text.primary" }}
          justifyContent="center"
        >
          <Typography variant="subtitle2">Dual {props.level}-Back</Typography>
        </Grid>

        <Grid
          container
          item
          xs={4}
          sx={{ color: "text.primary" }}
          justifyContent="center"
        >
          {props.state == States.Running ? (
            <IconButton onClick={props.stopNback}>
              <StopIcon sx={{ color: "secondary.main" }} />
            </IconButton>
          ) : (
            <IconButton
              onClick={props.initNback}
              disabled={props.state !== States.Idle}
              sx={{
                "&.disabled": { color: "primary.main" },
                color: "secondary.main",
              }}
            >
              <PlayArrowIcon />
            </IconButton>
          )}

          <IconButton
            onClick={handleMenuClick}
            color="inherit"
            disabled={props.state == States.Running}
            sx={{
              "&.disabled": { color: "primary.main" },
              color: "secondary.main",
            }}
          >
            <MenuIcon />
          </IconButton>

          <Menu
            id="optionsMenu"
            anchorEl={anchorMenuEl}
            open={Boolean(anchorMenuEl)}
            onClose={handleMenuClose}
            onClick={handleMenuClose}
          >
            <MenuItem
              onClick={() => {
                props.setState(States.Help);
                setOpenHelp(true);
                handleMenuClose();
              }}
            >
              <ListItemIcon
                onClick={() => {
                  props.setState(States.Help);
                  setOpenHelp(true);
                }}
                sx={{
                  color: "secondary.main",
                }}
              >
                <HelpIcon fontSize="small" />
              </ListItemIcon>
              <Typography variant="subtitle2">Help</Typography>
            </MenuItem>
            <MenuItem
              onClick={() => {
                props.setState(States.Insights);
                setOpenInsights(true);
                handleMenuClose();
              }}
            >
              <ListItemIcon
                onClick={() => {
                  props.setState(States.Insights);
                  setOpenInsights(true);
                }}
                sx={{
                  color: "secondary.main",
                }}
              >
                <InsightsIcon fontSize="small" />
              </ListItemIcon>
              <Typography variant="subtitle2">Charts</Typography>
            </MenuItem>
            <MenuItem
              onClick={() => {
                props.setState(States.Settings);
                setOpenSettings(true);
                handleMenuClose();
              }}
            >
              <ListItemIcon
                sx={{
                  color: "secondary.main",
                }}
              >
                <SettingsIcon fontSize="small" />
              </ListItemIcon>
              <Typography variant="subtitle2">Settings</Typography>
            </MenuItem>
            <MenuItem
              onClick={() => {
                mode == "light" ? setMode("dark") : setMode("light");
                handleMenuClose();
              }}
            >
              <ListItemIcon
                sx={{
                  color: "secondary.main",
                }}
              >
                {mode == "light" ? (
                  <DarkModeIcon
                    fontSize="small"
                    sx={{ color: "secondary.main" }}
                  />
                ) : (
                  <LightModeIcon
                    fontSize="small"
                    sx={{ color: "secondary.main" }}
                  />
                )}
              </ListItemIcon>
              <Typography variant="subtitle2">Theme</Typography>
            </MenuItem>
          </Menu>
        </Grid>
      </Grid>

      <SettingsDialog
        open={openSettings}
        handleClose={handleSettingsClose}
        settings={props.settings}
        setSettings={props.setSettings}
      />

      <HelpDialog open={openHelp} handleClose={handleHelpClose} />

      <ChartsDialog
        open={openInsights}
        handleClose={handleInsightsClose}
        sessions={props.sessions}
        settings={props.settings}
      />

      <TargetReachedDialog
        open={openTargetReached}
        handleClose={handleTargetReachedClose}
      />
    </>
  );
}

export default TopBar;
